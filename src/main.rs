//! This CLI utility allows hashing some data the same way a substrate-based chain does.
//! That allows, for instance, to calculate the hash of a runtime wasm blob which will
//! match the proposal hash you can find on-chain.

extern crate clap;

use clap::{crate_version, App, Arg};
use std::fs::File;
use std::io;
use std::{io::Read, process::exit};
use substrate_runtime_hasher::{get_result, SrhResult};

const _1KB: usize = 1024;
const BUFFER_SIZE: usize = 8 * _1KB;

/// Main function of our CLI
fn main() -> io::Result<()> {
    let app = App::new("substrate-runtime-hasher");
    let mut app_clone = app.clone();
    let matches = &app
        .version(crate_version!())
        .author("Wilfried Kopp <chevdor@gmail.com>")
        .about(
            "Hash data the same way Substrate-based chains such as Kusama or Polkadot. \
            \nThis gives a hash matching proposal hashes onchain.",
        )
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file to use. Alternatively, you may pipe <stdin>.")
                .index(1),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    // If a filename was passed, we read from there. If not, we
    // read stdin.
    let buffer: Vec<u8> = match matches.value_of("INPUT") {
        Some(filename) => {
            let mut buf: Vec<u8> = Vec::with_capacity(BUFFER_SIZE);
            let mut f = File::open(filename)?;
            f.read_to_end(&mut buf)?;
            buf
        }
        None => {
            let stdin = io::stdin();
            let mut stdin = stdin.lock();

            let mut buf: Vec<u8> = Vec::with_capacity(BUFFER_SIZE);
            stdin.read_to_end(&mut buf)?;

            if buf.len() == 0 || buf == &[10] {
                app_clone.print_help().expect("Failed showing help");
                println!("\n🤚 If you pass data using stdin, it must not be empty!");
                exit(1);
            }

            buf.iter().cloned().collect()
        }
    };

    if matches.occurrences_of("v") > 0 {
        let size = buffer.len();
        println!(
            "buffer size: {} bytes ({:.3} Mb)",
            size,
            (size as f64) / 1024f64 / 1024f64
        );
    }

    let result: SrhResult = get_result(&buffer);
    println!("0x{}", &result.encodedd_hash);

    Ok(())
}
