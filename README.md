# `substrate-runtime-hasher`

When a candidate runtime is submitted to a Substrate-based chain such as Polkadot or Substrate, this WASM blob is hashed.
The proposal hash that can be seen is actually the prefixed and hashed runtime. This utility does the same and allows calculating
the proposal hash of a new runtime off-chain.

You do not need a node, nor even an internet connection to use this utility.

The rustdoc can be found [here](https://chevdor.gitlab.io/substrate-runtime-hasher/substrate_runtime_hasher).

This is the documentation for `substrate-runtime-hasher v0.2.9`.

## Install

        cargo install --git https://gitlab.com/chevdor/substrate-runtime-hasher.git --tag v0.2.9

Installing the version from \`master\` is not recommended for production: do \*NOT\* omit the \`--tag v0.2.9\` in the previous command.

## Usage

### Sample run

In the following examples, the path of the WASM file will be stored into a variables called `WASM`.

You may set it with:

    export WASM=/home/foobar/kusama/target/srtool/release/wbuild/kusama-runtime/kusama_runtime.compact.wasm

**Build your runtime…​ or let [srtool](https://gitlab.com/chevdor/srtool) do it.**

        cd <polkadot-repo>
        # build the runtime (you could use srtool)

**Invoke `substrate-runtime-hasher`.**

        substrate-runtime-hasher $WASM
        0x5931690e71e9d3d9f04a43d8c15e45e0968e563858dd87ad6485b2368a286a8f

If you are using [srtool](https://gitlab.com/chevdor/srtool), it will do everything for you. The default location for the WASM will be inside your repo in `$WASM` (for instance, for kusama).

### Unix pipes & input redirection

Since `v0.2.1`, you can also use Unix pipes:

**Piping into substrate-runtime-hasher.**

        cat $WASM | substrate-runtime-hasher
        0x5931690e71e9d3d9f04a43d8c15e45e0968e563858dd87ad6485b2368a286a8f

**Input redirection.**

        substrate-runtime-hasher < $WASM
        0x5931690e71e9d3d9f04a43d8c15e45e0968e563858dd87ad6485b2368a286a8f

If you are using commands such as `echo` make sure to **NOT** send an extra `newline`. For instance `echo "123" | substrate-runtime-hasher` will not hash `123` but `123<cr>`. In that case, you probably want to use `echo -n "123" | substrate-runtime-hasher`

### Help!

        substrate-runtime-hasher --help

You should see an output similar to:

        substrate-runtime-hasher 0.2.1
        Wilfried Kopp <chevdor@gmail.com>
        Hash data the same way Substrate-based chains such as Kusama or Polkadot.
        This gives a hash matching proposal hashes onchain.

        USAGE:
            substrate-runtime-hasher [FLAGS] [INPUT]

        FLAGS:
            -h, --help       Prints help information
            -v               Sets the level of verbosity
            -V, --version    Prints version information

        ARGS:
            <INPUT>    Sets the input file to use. Alternatively, you may pipe <stdin>.

## References

-   srtool articles: <https://www.chevdor.com/tags/srtool>

-   srtool repo: <https://gitlab.com/chevdor/srtool>

## License

    MIT License

    Copyright (c) 2019-2020 Wilfried Kopp - Chevdor

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
